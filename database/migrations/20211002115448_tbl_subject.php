<?php

/**
 * MIGRATION DOCUMENTATION
 * https://sprnva.000webhostapp.com/docs/migration
 *
 * Always remember:
 * "up" is for run migration
 * "down" is for the rollback, reverse the migration
 * 
 */
$tbl_subject = [
	"mode" => "NEW",
	"table"	=> "tbl_subject",
	"primary_key" => "subject_id",
	"up" => [
		"subject_id" => "INT(11) NOT NULL AUTO_INCREMENT",
		"subject_name" => "VARCHAR(50) DEFAULT NULL",
		"subject_desc" => "TEXT DEFAULT NULL",
		"subject_added_by" => "INT(11) DEFAULT NULL",
	],
	"down" => [
		"" => ""
	]
];
