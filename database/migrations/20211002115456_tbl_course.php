<?php

/**
 * MIGRATION DOCUMENTATION
 * https://sprnva.000webhostapp.com/docs/migration
 *
 * Always remember:
 * "up" is for run migration
 * "down" is for the rollback, reverse the migration
 * 
 */
$tbl_course = [
	"mode" => "NEW",
	"table"	=> "tbl_course",
	"primary_key" => "course_id",
	"up" => [
		"course_id" => "INT(11) NOT NULL AUTO_INCREMENT",
		"course_name" => "VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci'",
		"course_desc" => "TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci'",
		"course_added_by" => "INT(11) NULL DEFAULT NULL",
	],
	"down" => [
		"" => ""
	]
];
