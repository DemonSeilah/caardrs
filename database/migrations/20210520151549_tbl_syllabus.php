<?php

/**
 * MIGRATION DOCUMENTATION
 * https://sprnva.000webhostapp.com/docs/migration
 *
 * Always remember:
 * "up" is for run migration
 * "down" is for the rollback, reverse the migration
 * 
 */
$tbl_syllabus = [
	"mode" => "NEW",
	"table"	=> "tbl_syllabus",
	"primary_key" => "s_id",
	"up" => [
		"s_id"  => "int(11) unsigned NOT NULL AUTO_INCREMENT",
		"s_course_code" => "varchar(50) DEFAULT NULL",
		"s_course_title" => "varchar(50) DEFAULT NULL",
		"s_credit_units" => "float DEFAULT NULL",
		"s_hours_week" => "varchar(50) DEFAULT NULL",
		"sched_id" => "int(11) DEFAULT NULL",
		"s_privilege" => "int(1) DEFAULT NULL",
		"s_desc" => "varchar(50) DEFAULT NULL",
		"s_date_added" => "date DEFAULT NULL",
		"s_added_by" => "int(11) DEFAULT NULL",
	],
	"down" => [
		"" => ""
	]
];
