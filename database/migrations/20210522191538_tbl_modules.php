<?php

/**
 * MIGRATION DOCUMENTATION
 * https://sprnva.000webhostapp.com/docs/migration
 *
 * Always remember:
 * "up" is for run migration
 * "down" is for the rollback, reverse the migration
 * 
 */
$tbl_modules = [
	"mode" => "NEW",
	"table"	=> "tbl_modules",
	"primary_key" => "m_id",
	"up" => [
		"m_id"  => "int(11) unsigned NOT NULL AUTO_INCREMENT",
		"s_id" => "int(11) DEFAULT NULL",
		"m_name" => "varchar(50) DEFAULT NULL",
		"m_privilege" => "int(1) DEFAULT NULL",
		"m_desc" => "varchar(50) DEFAULT NULL",
		"m_date_added" => "date DEFAULT NULL",
		"m_added_by" => "int(11) DEFAULT NULL",
		"m_slug" => "text DEFAULT NULL",
	],
	"down" => [
		"" => ""
	]
];
