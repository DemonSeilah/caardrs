<?php

/**
 * --------------------------------------------------------------------------
 * Routes
 * --------------------------------------------------------------------------
 * 
 * Here is where you can register routes for your application.
 * Now create something great!
 * 
 */

use App\Core\Routing\Route;

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/home', ['WelcomeController@home', ['auth']]);

// Schedule
Route::get('/schedule/home', ['ScheduleController@schedule', ['auth']]);

// Variables / route    
Route::post('/schedule/save', ['ScheduleController@save', ['auth']]);
Route::post('/schedule/delete', ['ScheduleController@delete', ['auth']]);
Route::post('/schedule/edit', ['ScheduleController@edit', ['auth']]);
Route::post('/schedule/update', ['ScheduleController@update', ['auth']]);

// All files
Route::get('/allfiles/home', ['AllFilesController@allfiles', ['auth']]);
Route::get('/allfiles/view/{id}', ['AllFilesController@view_specfiles', ['auth']]);

// Variables / route   
Route::post('/allfiles/save', ['AllFilesController@save', ['auth']]);
Route::post('/allfiles/save/module', ['AllFilesController@save_module', ['auth']]);
Route::post('/allfiles/rename/module', ['AllFilesController@rename_module', ['auth']]);
Route::post('/allfiles/delete', ['AllFilesController@delete', ['auth']]);
Route::post('/allfiles/delete_bulk_module', ['AllFilesController@delete_bulk_module', ['auth']]);
Route::post('/allfiles/delete_specific_module', ['AllFilesController@delete_specific_module', ['auth']]);

// Modules
Route::get('/modules/view/{id}', ['ModulesController@view_module', ['auth']]);

// Upload 
Route::post('/modules/upload/{code}', ['ModulesController@uploadFile', ['auth']]);

// User
Route::get('/user/home', ['UserController@user', ['auth']]);
Route::post('/user/save', ['UserController@save', ['auth']]);
Route::post('/user/edit', ['UserController@edit', ['auth']]);
Route::post('/user/delete', ['UserController@delete', ['auth']]);

// Reports
Route::get('/report', ['ReportController@report', ['auth']]);

//Subject & Course
Route::get('/sc/subject', ['SubjectCourseController@subject', ['auth']]);
Route::post('/sc/add', ['SubjectCourseController@add', ['auth']]);
Route::post('/sc/delete', ['SubjectCourseController@delete', ['auth']]);
Route::post('/sc/edit', ['SubjectCourseController@edit', ['auth']]);
Route::post('/sc/update', ['SubjectCourseController@update', ['auth']]);
Route::get('/sc/course', ['SubjectCourseController@course', ['auth']]);
Route::post('/sc/c_add', ['SubjectCourseController@c_add', ['auth']]);
Route::post('/sc/c_edit', ['SubjectCourseController@c_edit', ['auth']]);
Route::post('/sc/c_delete', ['SubjectCourseController@c_delete', ['auth']]);
Route::post('/sc/c_update', ['SubjectCourseController@c_update', ['auth']]);
