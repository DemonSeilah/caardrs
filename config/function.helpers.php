<?php

use App\Core\App;

function getPrivilege($type)
{
    return ($type == 0 ? "All User" : "Private");
}

function getUser($id)
{
    $user_data = DB()->select("*", 'users', "id='$id'")->get();
    return $user_data["fullname"];
}

function getProgram($id)
{
    $user_data = DB()->select("*", 'tbl_schedule', "sched_id='$id'")->get();
    return $user_data["sched_course"];
}

function getRole($id)
{
    $user_data = DB()->select("*", 'users', "id='$id'")->get();

    if ($user_data['role_id'] == 1) {
        $role_name = "Dean";
    } else if ($user_data['role_id'] == 2) {
        $role_name = "Program Chair Person";
    } else if ($user_data['role_id'] == 3) {
        $role_name = "Faculty";
    }

    return $role_name;
}

function checkIfImage($filetype)
{
    $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");

    if (in_array($filetype, $image_icons_array)) {
        $response = true;
    } else {
        $response = false;
    }

    return $response;
}

function getImageView($filetype, $path)
{
    $logo_path = public_url("/assets/sprnva/file_extension_icon/");
    $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");
    $file_icons_array = array("XLS", "DOCX", "CSV", "TXT", "ZIP", "EXE", "XLSX", "PPT", "PPTX", "PDF");

    if (in_array($filetype, $image_icons_array)) {
        $icon = public_url("/../" . $path);
    } else {
        if (in_array($filetype, $file_icons_array)) {
            $icon = $logo_path . $filetype . '.png';
        } else {
            $icon = $logo_path . 'FILE.png';
        }
    }

    return $icon;
}
