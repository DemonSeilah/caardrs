<?php

use App\Core\App;
use App\Core\Auth;
?>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel='icon' href='<?= public_url('/favicon.ico') ?>' type='image/ico' />
	<title>
		<?= ucfirst($pageTitle) . " | " . App::get('config')['app']['name'] ?>
	</title>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/bootstrap/css/bootstrap.min.css') ?>">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/assets/icon/feather/css/feather.css') ?>">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/datatables.net-bs4/css/dataTables.bootstrap4.min.css') ?>">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/assets/pages/data-table/css/buttons.dataTables.min.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/assets/css/jquery.mCustomScrollbar.css') ?>">
	<!-- themify-icons line icon -->
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/icon/themify-icons/themify-icons.css') ?>">
	<!-- ico font -->
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/icon/icofont/css/icofont.css') ?>">
	<!-- feather Awesome -->
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/icon/feather/css/feather.css') ?>">
	<!-- font Awesome -->
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/icon/font-awesome/css/font-awesome.min.css') ?>">
	<!-- sweetalerts -->
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/sweetalert/css/sweetalert.css') ?>">
	<!-- animate  -->
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/animate.css/css/animate.css') ?>">
	<!-- components -->
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/css/component.css') ?>">
	<!-- notificaitons -->
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/pages/notification/notification.css') ?>">
	<!-- animate -->
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/animate.css/css/animate.css') ?>">
	<!-- switchery -->
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/switchery/css/switchery.min.css') ?>">
	<!-- bootstrap input -->
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/bootstrap-tagsinput/css/bootstrap-tagsinput.css') ?>">
	<!-- toolbars -->
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/pages/toolbar/jquery.toolbar.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/assets/pages/toolbar/custom-toolbar.css') ?>">
	<link rel="stylesheet" href="<?= public_url('/assets/adminty/select2/css/select2.min.css') ?>" />
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/bootstrap-multiselect/css/bootstrap-multiselect.css') ?>" />
	<link rel="stylesheet" type="text/css" href="<?= public_url('/assets/adminty/multiselect/css/multi-select.css') ?>" />

	<link rel=" stylesheet" href="<?= public_url('/assets/adminty/assets/css/style.css') ?>">
	<style>
		/* body {
			background-color: #eef1f4;
			font-weight: 350;
		} */
	</style>
	<!-- <link href="vendor/select2/dist/css/select2.min.css" rel="stylesheet" />
	<script src="vendor/select2/dist/js/select2.min.js"></script> -->

	<script src="<?= public_url('/assets/adminty/jquery/js/jquery.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/jquery-ui/js/jquery-ui.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/popper.js/js/popper.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/bootstrap/js/bootstrap.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/jquery-slimscroll/js/jquery.slimscroll.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/modernizr/js/modernizr.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/modernizr/js/css-scrollbars.jss') ?>"></script>
	<!-- file upload -->
	<script src="<?= public_url('/assets/adminty/assets/pages/jquery.filer/js/jquery.filer.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/assets/pages/filer/custom-filer.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/assets/pages/filer/jquery.fileuploads.init.js') ?>"></script>

	<!-- toolbars -->
	<script src="<?= public_url('/assets/adminty/assets/pages/toolbar/jquery.toolbar.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/assets/pages/toolbar/custom-toolbar.js') ?>"></script>


	<!-- datatables -->
	<script src="<?= public_url('/assets/adminty/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/datatables.net-buttons/js/buttons.print.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/datatables.net-buttons/js/buttons.html5.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/datatables.net-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/datatables.net-responsive/js/dataTables.responsive.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') ?>"></script>


	<!-- Chart js -->
	<script src="<?= public_url('/assets/adminty/chart.js/js/Chart.js') ?>"></script>
	<!-- amchart js -->
	<script src="<?= public_url('/assets/adminty/assets/pages/widget/amchart/amcharts.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/pages/widget/amchart/serial.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/assets/pages/widget/amchart/light.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/assets/js/jquery.mCustomScrollbar.concat.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/assets/js/SmoothScroll.js') ?>"></script>
	<!-- sweetalert -->
	<script src="<?= public_url('/assets/adminty/sweetalert/js/sweetalert.min.js') ?>"></script>
	<!-- modals -->
	<script src="<?= public_url('/assets/adminty/assets/js/modal.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/assets/js/modalEffects.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/assets/js/classie.js') ?>"></script>
	<!-- pcoded for theme -->
	<script src="<?= public_url('/assets/adminty/assets/js/pcoded.min.js') ?>"></script>
	<!-- custom js -->
	<script src="<?= public_url('/assets/adminty/assets/js/vartical-layout.min.js'); ?>"></script>
	<!-- <script src="<?= public_url('/assets/adminty/assets/pages/dashboard/custom-dashboard.js') ?>"></script> -->


	<!-- notifications -->
	<script src="<?= public_url('/assets/adminty/assets/js/bootstrap-growl.min.js') ?>"></script>
	<script src="<?= public_url('/assets/adminty/assets/pages/notification/notification.js') ?>"></script>
	<!-- switches -->
	<script src="<?= public_url('/assets/adminty/assets/pages/advance-elements/swithces.js') ?>"></script>

	<script src="<?= public_url('/assets/adminty/assets/js/script.min.js') ?>"></script>
	<script type="text/javascript" src="<?= public_url('/assets/adminty/select2/js/select2.full.min.js') ?>"></script>
	<script type="text/javascript" src="<?= public_url('/assets/adminty/bootstrap-multiselect/js/bootstrap-multiselect.js') ?>"></script>
	<script type="text/javascript" src="<?= public_url('/assets/adminty/multiselect/js/jquery.multi-select.js') ?>"></script>
	<script type="text/javascript" src="<?= public_url('/assets/adminty/assets/js/jquery.quicksearch.js') ?>"></script>
	<script type="text/javascript" src="<?= public_url('/assets/adminty/assets/pages/advance-elements/select2-custom.js') ?>"></script>
	<?php require_once __DIR__ . '/filepond.php'; ?>

	<script>
		const base_url = "<?= App::get('base_url') ?>";
	</script>

</head>

<body>
	<!-- Pre-loader start -->
	<div class="theme-loader">
		<div class="ball-scale">
			<div class='contain'>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Pre-loader end -->

	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">

			<nav class="navbar header-navbar pcoded-header">
				<div class="navbar-wrapper">

					<div class="navbar-logo">
						<a class="mobile-menu" id="mobile-collapse" href="#!">
							<i class="feather icon-menu"></i>
						</a>
						<a href="index-1.htm">
							<h5>CAARDRS</h5>
							<!-- <img class="img-fluid" src="/assets/adminty/assets/images/logo.png" alt="Theme-Logo"> -->
						</a>
						<a class="mobile-options">
							<i class="feather icon-more-horizontal"></i>
						</a>
					</div>

					<div class="navbar-container container-fluid">
						<ul class="nav-left">
							<li>
								<a href="#!" onclick="javascript:toggleFullScreen()">
									<i class="feather icon-maximize full-screen"></i>
								</a>
							</li>
						</ul>
						<ul class="nav-right">

							<li class="user-profile header-notification">
								<div class="dropdown-primary dropdown">
									<div class="dropdown-toggle" data-toggle="dropdown">
										<img src="<?= public_url('/assets/adminty/assets/images/avatar-4.jpg') ?>">
										<span><?= Auth::user('fullname') ?></span>
										<i class="feather icon-chevron-down"></i>
									</div>
									<ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
										<?php if (Auth::user('role_id') == 1) { ?>
											<li>
												<a href="<?= route('/user/home') ?>">
													<i class="fa fa-user-plus"></i>Manage User
												</a>
											</li>
										<?php } ?>
										<li>
											<a href="<?= route('/sc/subject') ?>">
												<i class="fa fa-file"></i>Manage Subject
											</a>
										</li>
										<li>
											<a href="<?= route('/sc/course') ?>">
												<i class="fa fa-tasks"></i>Manage Course
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-user"></i> Profile
											</a>
										</li>
										<li>
											<a href="<?= route('/logout') ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
												<i class="fa fa-sign-out"></i> Logout
											</a>

											<form id="logout-form" action="<?= route('/logout') ?>" method="POST" style="display:none;">
												<?= csrf() ?>
											</form>
										</li>
									</ul>

								</div>
							</li>
						</ul>
					</div>
				</div>
			</nav>


			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<nav class="pcoded-navbar" navbar-theme="themelight1" active-item-theme="themelight1" sub-item-theme="theme2" active-item-style="style0" pcoded-navbar-position="fixed">
						<div class="pcoded-inner-navbar main-menu">
							<div class="pcoded-navigatio-lavel">Navigation</div>
							<ul class="pcoded-item pcoded-left-item">
								<li class="">
									<a href="<?= route('/') ?>">
										<span class="pcoded-micon"><i class="feather icon-home"></i></span>
										<span class="pcoded-mtext">Home</span>
									</a>
								</li>
								<?php if (Auth::user('role_id') == 1) { ?>
									<li class="">
										<a href="<?= route('/schedule/home') ?>">
											<span class="pcoded-micon"><i class="feather icon-file-text"></i></span>
											<span class="pcoded-mtext">Assign Load</span>
											<!-- <span class="pcoded-badge label label-warning">NEW</span> -->
										</a>

									</li>
								<?php } ?>
								<li class="">
									<a href="<?= route('/allfiles/home') ?>">
										<span class="pcoded-micon"><i class="feather icon-folder"></i></span>
										<span class="pcoded-mtext">Syllabus</span>
										<!-- <span class="pcoded-badge label label-warning">NEW</span> -->
									</a>

								</li>
								<?php if (Auth::user('role_id') == 1) { ?>
									<li class="">
										<a href="<?= route('/report') ?>">
											<span class="pcoded-micon"><i class="feather icon-book"></i></span>
											<span class="pcoded-mtext">Reports</span>
											<!-- <span class="pcoded-badge label label-warning">NEW</span> -->
										</a>

									</li>
								<?php } ?>
							</ul>
						</div>
					</nav>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">

									<!-- Page-header start -->
									<!-- <div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-12">
												<div class="page-header-title">
													<div class="d-inline">

													</div>
												</div>
											</div>
										</div>
									</div> -->
									<!-- Page-header end -->
									<div class="page-body">

										<div class="row" style="margin-top: -34px;">