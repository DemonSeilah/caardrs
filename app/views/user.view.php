<?php

use App\Core\Auth;

require 'layouts/head.php'; ?>
<div class="col-lg-12">
    <div class="col-md-8" style="margin: 0 auto; margin-bottom: 30px;">
        <img style="width: inherit;" src="<?= public_url('/assets/adminty/assets/images/bannerlogo.png') ?>" alt="banner-logo">
    </div>
    <div class="row align-items-end" style="margin-bottom: 10px;">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4><?= $pageTitle ?></h4>
                    <!-- <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span> -->
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="<?= route('/') ?>"> <i class="feather icon-home"></i> </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!"><?= $pageTitle ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Zero config.table start -->
<div class="col-lg-12">
    <?= alert_msg(); ?>
    <div class="card">
        <div class="card-header">
            <button class="btn btn-out btn-danger btn-square waves-effect md-trigger float-right" onclick="deleteUser()"><i class="fa fa-trash"></i>Delete User</button>
            <button class="btn btn-out btn-success btn-square waves-effect md-trigger float-right" onclick="addUser()"><i class="fa fa-plus-circle"></i>Add User</button>
        </div>
        <div class="col-md-11" style="border-bottom: 2px dashed #0ac282;margin: 0 auto;"></div>
        <div class="card-block">

            <div class="dt-responsive table-responsive">
                <table id="tbl_schedule" class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th style="width: 36px;  border-top-left-radius: 16px !important; padding-bottom: 20px;">
                                <div class="checkbox-color checkbox-success">
                                    <input id="checkbox18" type="checkbox" id="checkboxAll" name="cbDAll" onclick="checkAll()">
                                    <label for="checkbox18" for="checkboxAll">

                                    </label>
                                </div>
                            </th>
                            <th>#</th>
                            <th></th>
                            <th>Full Name</th>
                            <th>Username</th>
                            <th>Email Address</th>
                            <th>Role</th>
                            <th style="border-top-right-radius: 16px !important;">Date Added</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $counter = 1;
                        foreach ($details as $data) : ?>
                            <tr>
                                <td>
                                    <!-- <div class="border-checkbox-section">
                                        <div class="border-checkbox-group border-checkbox-group-success">
                                            <input class="border-checkbox" type="checkbox" id="checkbox<?= $data['id']; ?>" name="cbD" value="<?= $data['id']; ?>">
                                            <label class="border-checkbox-label" for="checkbox<?= $data['id']; ?>"></label>
                                        </div>
                                    </div> -->
                                    <div class="checkbox-color checkbox-success">
                                        <input type="checkbox" id="checkbox<?= $data['id']; ?>" name="cbD" value="<?= $data['id']; ?>">
                                        <label for="checkbox<?= $data['id']; ?>">

                                        </label>
                                    </div>
                                </td>
                                <td><?= $counter++; ?></td>
                                <td>

                                    <div id="toolbar-options<?= $data['id']; ?>" class="hidden">
                                        <a href="#" onclick="editUser(<?= $data['id']; ?>)"><i class="fa fa-pencil"></i></a>
                                        <!-- <a href="#"><i class="fa fa-eye"></i></a> -->
                                    </div>
                                    <div class="tool-box">
                                        <div data-toolbar="user-options" class="btn-toolbar btn-success btn-toolbar-success test" id="left-toolbar<?= $data['id']; ?>" onmouseover="toolbar(<?= $data['id']; ?>)"><i class="fa fa-cog"></i></div>
                                        <div class="clear"></div>
                                    </div>
                                </td>
                                <td><?= $data['fullname']; ?></td>
                                <td><?= $data['username']; ?></td>
                                <td><?= $data['email']; ?></td>
                                <td><?= $data['role_id']; ?></td>
                                <td><?= date("Y-m-d", strtotime($data['created_at'])); ?></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>

                </table>
            </div>
        </div>
        <!-- Zero config.table end -->


    </div>

    <?php include_once __DIR__ . '/modals/add_user_modals.php'; ?>
    <?php include_once __DIR__ . '/modals/edit_user_modal.php'; ?>
    <script>
        $(document).ready(function() {
            getScheduleTable();


        });

        function toolbar(id) {
            // alert(id);
            $('#left-toolbar' + id).toolbar({
                content: '#toolbar-options' + id,
                position: "left",
                style: 'success'
            });
        }

        function getScheduleTable() {
            // alert("test");

            $("#tbl_schedule").DataTable();
        }

        function addUser() {
            //alert('test');
            $("#adduserModal").modal();
        }

        function editUser(id) {
            // alert("test");
            $("#edituserModal").modal();
            url = base_url + "/user/edit";

            $.post(url, {
                id: id
            }, function(data) {
                var dd = JSON.parse(data);


                // console.log(dd);
                $("#user_id").val(dd.id);
                $("#user_fullname").val(dd.fullname);
                $("#user_username").val(dd.username);
                $("#user_email").val(dd.email);
                $("#user_role").val(dd.role_id);
                //  alert(dd.sched_start_time);
            });

        }
        $('#frmAdduser').submit(function(e) {
            e.preventDefault();
            var url = base_url + '/user/save';
            var data = $('#frmAdduser').serialize();
            $.post(url, data, function(data) {
                if (data == 1) {
                    // alert(data);

                    notify("top", "right", "fa fa-check", "success", "animated bounceInRight", "animated bounceOutRight", "<b>Success!</b> ", " Data has been updated.");
                    $("#adduserModal").modal('hide');
                    setTimeout(function() {
                        location.reload();
                    }, 1000);
                } else {
                    notify("top", "right", "fa fa-check", "danger", "animated bounceInRight", "animated bounceOutRight", "<b>Aw snap!</b> ", "Email or username already exist.");
                }

            });
        });

        $('#frmEditSchedule').submit(function(e) {
            e.preventDefault();
            var url = base_url + '/schedule/update';
            var data = $('#frmEditSchedule').serialize();
            $.post(url, data, function(data) {
                if (data == 1) {
                    location.reload();
                    notify("top", "right", "fa fa-check", "success", "animated bounceInRight", "animated bounceOutRight", "<b>Success!</b> ", " Record deleted.");
                    $("input[name=cbDAll]").prop("checked", false);
                }
            });
        });

        function checkAll() {
            if ($("input[name=cbDAll]").is(":checked")) {
                $("input[name=cbD]").prop("checked", true);

            } else {
                $("input[name=cbD]").prop("checked", false);
            }

        }

        function deleteUser() {
            // alert("test");
            var popUp = confirm("Are you sure you want to delete this entry??");
            if (popUp == true) {
                if ($("input[name=cbD]").is(":checked")) {
                    var id = [];
                    var id = $("input[name=cbD]:checked").map(function() {
                        return $(this).val();
                    }).get();

                    //alert(id);

                    $.ajax({
                        type: "POST",
                        url: base_url + "/user/delete",
                        data: {
                            id: id
                        },
                        success: function(data) {
                            //alert(data);
                            if (data == 1) {
                                location.reload();
                                notify("top", "right", "fa fa-check", "success", "animated bounceInRight", "animated bounceOutRight", "<b>Success!</b> ", " Record deleted.");
                                $("input[name=cbDAll]").prop("checked", false);
                            }
                        }
                    });

                } else {
                    notify("top", "right", "fa fa-check", "warning", "animated bounceInRight", "animated bounceOutRight", "<b>Aw Snap!</b> ", " No entry selected.");

                }
            } else {
                notify("top", "right", "fa fa-check", "warning", "animated bounceInRight", "animated bounceOutRight", "<b>Aw Snap!</b> ", " You've cancel it!");
            }
        }
    </script>
    <?php require 'layouts/footer.php'; ?>