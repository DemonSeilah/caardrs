<?php

use App\Core\Auth;
use App\Core\Request;


require 'layouts/head.php'; ?>

<style>
    #cardID:hover {
        background: #eaf2ff;
        cursor: pointer;
    }

    #cardID {
        margin-right: 12px;
    }

    #m_menu:hover {
        /* color: green; */
        background: #c3c3c3;
        border-radius: 9px;
    }

    #m_menu:active {
        color: white;
        background: grey;
        border-radius: 9px;
    }

    #m_menu {
        width: 25px;
        position: absolute;
        margin-left: 79%;
        margin-top: -1px;
        text-align: center;
        background: transparent;
        border: 0;
        color: grey;
    }

    .dropdown-toggle::after {
        display: none !important;
    }
</style>

<!-- Zero config.table start -->
<div class="col-lg-12">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4><?= $pageTitle ?></h4>
                    <!-- <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span> -->
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="<?= route('/') ?>"> <i class="feather icon-home"></i> Home</a>
                    </li>
                    <?= $breadcrumbs; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="card borderless-card">
        <div class="card-block inverse-breadcrumb">
            <div class="breadcrumb-header">
                <h5><?= $module_data["m_name"] ?></h5>
                <span><?= $module_data["m_desc"] ?></span>
                <span><?= date('F d, Y', strtotime($module_data["m_date_added"])) ?></span>
            </div>
            <div class="page-header-breadcrumb">
                <h5></h5>
            </div>
        </div>
    </div>
    <div>
        <button class="btn btn-inverse btn-icon pull-right" onclick="makeModal()" data-toggle="tooltip" data-placement="left" title="" data-original-title="Upload Files" style="position: absolute; right: 23px; box-shadow: 0px 2px 32px -3px rgb(34 34 34 / 75%); top: 86%; z-index: 1;  width: 60px;  height: 60px;
"><i style="margin-right: 0 !important; font-size: 26px;" class="fa fa-upload"></i></button>
        <button class="btn btn-danger btn-icon pull-right" onclick="" data-toggle="tooltip" data-placement="left" title="" data-original-title="Delete Files" style="position: absolute; right: 99px; box-shadow: 0px 2px 32px -3px rgb(34 34 34 / 75%); top: 86%; z-index: 1;  width: 60px;  height: 60px;
"><i style="margin-right: 0 !important; font-size: 26px;" class="fa fa-trash"></i></button>
    </div>
    <br>
    <br>
    <div class="col-lg-12" style="height: 400px; overflow: auto; padding: 30px; border-radius: 10px; position: static;">

        <?php
        if (count($files) > 0) {
        ?>

            <?php
            foreach ($files as $file) {
                $iconSize = "width: " . $file['iconsize'] . ";";

                if (!checkIfImage($file['filetype'])) {
                    $isNotImg = "display: inline-flex;flex-direction: row;word-break: break-all;";
                } else {
                    $isNotImg = "";
                }
            ?>

                <div class="col-md-12">
                    <div class="card float-left" style="width: 10rem; box-shadow: 3px 3px 8px #adadad; position: static; margin-right: 10px;" data-toggle="tooltip" data-placement="top" title="<?= $file['filename'] ?>" data-original-title="">
                        <img class="card-img-top" style="width: 160px; height: 160px; object-fit: cover;" src="<?= getImageView($file['filetype'], $file['slug']) ?>" alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text" style="white-space: nowrap; width: 84%;  overflow: hidden; text-overflow: ellipsis; margin: 0;  align-content: flex-end;"> <?= $file['filename'] ?></p>
                            <p class="card-text card-text-small d-flex flex-column"> <small class="text-muted" style="display: flex;flex-direction: row;justify-content: space-between;align-items: center;justify-items: center;">
                                    <?= number_format($file['filesize'], 2) . " MB | [{$file['filetype']}]" ?>

                                    <!-- <span class="d-flex flex-row show-onhover">
                                        <i class="fa fa-cog show-onhover text-muted" style="font-size: 14px;cursor: pointer;" onclick="fileOption('<?= $file['id'] ?>', '<?= $file['slug'] ?>', '<?= $file['filename'] ?>')"></i>
                                    </span> -->

                                    <div class="dropdown-inverse dropdown open">
                                        <span id="m_menu" onclick="fm_menu()" class="btn btn-disable dropdown-toggle waves-effect waves-light " type="button" id="dropdown-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i style="font-size: 22px;" class="fa fa-ellipsis-v"></i></span>
                                        <div class="dropdown-menu" aria-labelledby="dropdown-4" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                                            <a class="dropdown-item waves-light waves-effect" href="#" onclick="renameModule('<?= $module_data['m_id']; ?>','<?= $module_data['m_name']; ?>')">Rename</a>
                                            <a class="dropdown-item waves-light waves-effect" href="#" onclick="deleteSpecificModule('<?= $module_data['m_id']; ?>')">Delete</a>
                                            <a class="dropdown-item waves-light waves-effect" href="#">View Description</a>
                                        </div>
                                    </div>
                                </small></p>
                        </div>
                    </div>
                </div>

            <?php } ?>

        <?php } ?>
    </div>

    <?php require __DIR__ . '/modals/make_modal.view.php'; ?>
    <?php require __DIR__ . '/modals/file_option_modal.view.php'; ?>

    <script>
        $(document).ready(function() {
            make_modal_init();
        });

        function makeModal() {
            make_modal_init();
            $('#make_modal').modal({
                show: true,
                backdrop: 'static',
                keyboard: true
            });
        }

        function fileOption(id, path, name) {

            alert(id + " | " + path + " | " + name);

        }
    </script>
    <?php require 'layouts/footer.php'; ?>