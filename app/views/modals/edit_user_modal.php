<div class="modal fade show" id="edituserModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Schedule</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="frmEditUser">

                <div class="modal-body">
                    <input type="hidden" class="form-control" id="user_id" name="id" required>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Full Name</span>
                                <input type="text" class="form-control" id="user_fullname" name="fullname" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Username</span>
                                <input class="form-control" id="user_username" name="username" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Email Address</span>
                                <input type="email" class="form-control" id="user_email" name="email" required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Role</span>
                                <select name="role" id="user_role" class="form-control">
                                    <option>Please Choose:</option>
                                    <option value="1">Dean</option>
                                    <option value="2">Program Chair Person</option>
                                    <option value="3">Faculty</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light ">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>