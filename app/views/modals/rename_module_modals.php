<div class="modal fade show" id="renameModuleModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Rename Module</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="frmRenameModule">
                <div class="modal-body">
                <div class="row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Module</span>
                                <input type="text" hidden  class="form-control" id="m_id" name="m_id">
                                <input type="text" class="form-control" id="module_rename" name="module_rename" required>
                            </div>
                        </div>
                    </div>
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light ">Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>