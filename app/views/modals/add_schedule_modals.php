<div class="modal fade show" id="scheduleModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Course</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="frmAddSchedule">
                <div class="modal-body">
                    <div class="col-sm-12">

                        <span class="" id="basic-addon1" style="display:inline-block;"><strong></strong></span>
                        <select class="js-example-basic-multiple col-sm-12" multiple="multiple">
                            <option value="AL">Alabama</option>
                            <option value="WY">Wyoming</option>
                            <option value="WY">Coming</option>
                            <option value="WY">Hanry Die</option>
                            <option value="WY">John Doe</option>
                        </select>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light ">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>