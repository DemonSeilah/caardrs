<div class="modal fade show" id="syllabusModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Syllabus</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="frmAddSyllabus">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Course Code</span>
                                <input type="text" class="form-control" name="course_code" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Course Title</span>
                                <input type="text" class="form-control" name="course_title" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Prerequisite</span>
                                <input type="text" class="form-control" name="course_preq" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Credit Units</span>
                                <input type="text" class="form-control" name="course_credit" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Hours/ Week</span>
                                <input type="text" class="form-control" name="course_hrwk" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Program</span>
                                <select name="course_program" class="form-control">
                                    <option>Please Choose:</option>
                                    <?php foreach ($schedule_details_data as $data_sched) : ?>
                                        <option value="<?= $data_sched['sched_id']; ?>"><?= $data_sched['sched_course']; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light ">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>