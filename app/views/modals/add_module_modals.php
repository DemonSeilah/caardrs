<div class="modal fade show" id="moduleModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Module</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="frmAddModule">
                <div class="modal-body">
                <div class="row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Module</span>
                                <input type="text" hidden class="form-control" name="s_id" value="<?=$syllabus_data["s_id"] ?>">
                                <input type="text" class="form-control" name="module" required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Description</span>
                                <textarea class="form-control" style="resize: none;" name="description" required></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Privilege</span>
                                <select  name="module_privilege" class="form-control">
                                    <option>Please Choose:</option>
                                    <option value="0">All User</option>
                                    <option value="1">Private</option>
                                </select>
                            </div>
                        </div>

                    </div> -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light ">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>