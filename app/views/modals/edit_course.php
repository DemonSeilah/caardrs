<div class="modal fade show" id="editCoursetModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Course</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="frmEditCourse">
                <input type="hidden" class="form-control" name="course_id" id="id_course_id" required>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Course Name</span>
                                <input type="text" class="form-control" id="id_course_name" name="course_name" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Description</span>
                                <input type="text" class="form-control" name="course_desc" id="id_course_desc" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Save Changes</button>
                    </div>
            </form>
        </div>
    </div>
</div>