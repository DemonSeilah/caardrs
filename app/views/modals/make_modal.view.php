<?php

use App\Core\Request;
?>
<div class="modal fade" id="make_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="col-12">
                    <div class="form-group">
                        <input type="text" hidden id="current_module" value="<?= $module_data["m_id"] ?>" /> 
                        <label for="username">Upload Files to <?= $module_data["m_name"] ?></label>
                    </div>

                    <div class="content">
                        <div class="form-group d-flex flex-column file-bin" id="file_bin" style="position: relative;max-width: 30rem;">
                            <input type="file" id="upload_file" name="upload_file" multiple data-max-files="10">

                            <div class="row">
                                <div class="col-12 mt-3">
                                    <div class="d-flex flex-row justify-content-end">
                                        <button type="button" class="btn btn-secondary mr-1" onclick="close_make_modal()">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(document).ready(function() {
        selectOption();
    });

    function make_modal_init() {
        $("#file_bin").css({
            "cssText": "display: block !important"
        });
    }

    function selectOption() {
        $("#file_bin").css({
                "cssText": "display: block !important"
            });

            var m_id = $("#current_module").val();
            

            FilePond.setOptions({
                    server: {
                        url: base_url + '/modules/upload/' + m_id,
                        headers: {
                            'X-CSRF-TOKEN': '<?= Request::csrf_token() ?>'
                        }
                    }
                });

            FilePond.registerPlugin(
                FilePondPluginFileEncode,
                FilePondPluginFileValidateSize,
                FilePondPluginImageExifOrientation,
                FilePondPluginImagePreview
            );

            FilePond.create(document.querySelector('input[type="file"]'));
    }

    function close_make_modal() {
        location.reload();
    }

</script>