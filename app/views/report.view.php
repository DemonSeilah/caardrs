<?php

use App\Core\Auth;


require 'layouts/head.php'; ?>
<div class="col-lg-12">
    <div class="col-md-8" style="margin: 0 auto; margin-bottom: 30px;">
        <img style="width: inherit;" src="<?= public_url('/assets/adminty/assets/images/bannerlogo.png') ?>" alt="banner-logo">
    </div>
    <div class="row align-items-end" style="margin-bottom: 10px;">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4><?= $pageTitle ?></h4>
                    <!-- <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span> -->
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="<?= route('/') ?>"> <i class="feather icon-home"></i> </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!"><?= $pageTitle ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Zero config.table start -->
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <form class="form-inline">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="from">From:</label>
                    <div class="col-md-10">
                        <input type="date" class="form-control" id="from">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="to">To:</label>
                    <div class="col-md-10">
                        <input type="date" class="form-control" id="to">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-10">
                        <button class="btn btn-out btn-success btn-square waves-effect md-trigger float-right" onclick=""><i class="fa fa-plus-circle"></i>Generate</button>
                    </div>
                </div>

            </form>
        </div>

        <!-- <div class="col-md-11" style="border-bottom: 2px dashed #0ac282;margin: 0 auto;"></div> -->
        <div class="card-block">
            <div class="dt-responsive table-responsive">

            </div>
        </div>
        <!-- Zero config.table end -->


    </div>

    <?php require 'layouts/footer.php'; ?>