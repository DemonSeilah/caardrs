<?php

use App\Core\Auth;


require 'layouts/head.php'; ?>

<style>
    #cardID:hover {
        background: #eaf2ff;
        cursor: pointer;
    }

    #cardID {
        margin-right: 12px;
    }

    #m_menu:hover {
        background: #c3c3c3;
        border-radius: 9px;
    }

    #m_menu:active {
        color: white;
        background: grey;
        border-radius: 9px;
    }

    #m_menu {
        width: 25px;
        position: absolute;
        margin-left: 79%;
        margin-top: -1px;
        text-align: center;
        background: transparent;
        border: 0;
        color: grey;
    }

    .dropdown-toggle::after {
        display: none !important;
    }
</style>

<div class="col-lg-12">
    <div class="col-md-8" style="margin: 0 auto; margin-bottom: 30px;">
        <img style="width: inherit;" src="<?= public_url('/assets/adminty/assets/images/bannerlogo.png') ?>" alt="banner-logo">
    </div>
    <div class="row align-items-end" style="margin-bottom: 10px;">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4><?= $pageTitle ?></h4>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="<?= route('/') ?>"> <i class="feather icon-home"></i> Home </a>
                    </li>
                    <?= $breadcrumbs; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="col-xl-12">
        <table style="width: 100%;" class="table table-columned">
            <thead>
                <tr style="background: #414e67; color: white; font-weight: bold;">
                    <th colspan="4" style="text-align: center;">COURSE SYLLABUS</th>

                </tr>
                <tr>
                    <th style="background: #414e671f;">COURSE CODE</th>
                    <th><?= $syllabus_data["s_course_code"] ?></th>

                    <th style="background: #414e671f;">CREDIT UNITS</th>
                    <th><?= $syllabus_data["s_credit_units"] ?></th>
                </tr>
                <tr>
                    <th style="background: #414e671f;">COURSE TITLE</th>
                    <th><?= $syllabus_data["s_course_title"] ?></th>

                    <th style="background: #414e671f;">HOURS/ WEEK</th>
                    <th><?= $syllabus_data["s_hours_week"] ?></th>
                </tr>
                <tr>
                    <th style="background: #414e671f;">PREREQUISITE</th>
                    <th><?= $syllabus_data["s_desc"] ?></th>

                    <th style="background: #414e671f;"> PROGRAM</th>
                    <th><?= getProgram($syllabus_data["sched_id"]) ?></th>
                </tr>
            </thead>
        </table>
    </div>

    <div>
        <button class="btn btn-inverse btn-icon pull-right" onclick="addModule()" data-toggle="tooltip" data-placement="left" title="" data-original-title="Add Module" style="position: absolute; right: 23px; box-shadow: 0px 2px 32px -3px rgb(34 34 34 / 75%); top: 86%; z-index: 1;  width: 60px;  height: 60px;"><i style="margin-right: 0 !important; font-size: 26px;" class="fa fa-plus"></i></button>
        <button id="bulkDeleteBtn" class="btn btn-danger btn-icon pull-right" onclick="deleteBulkModule()" data-toggle="tooltip" data-placement="left" title="" data-original-title="Delete Module" style="position: absolute; right: 99px; box-shadow: 0px 2px 32px -3px rgb(34 34 34 / 75%); top: 86%; z-index: 1;  width: 60px;  height: 60px;"><i style="margin-right: 0 !important; font-size: 26px;" class="fa fa-trash"></i></button>
    </div>
    <br>
    <br>
    <div class="col-lg-12" style="height: 400px; overflow: auto;   padding: 30px; border-radius: 10px; position: static;">

        <div class="row">

            <?php if (count($syllabus_details_data) != 0) {

                foreach ($syllabus_details_data as $modules_data) { ?>
                    <div class="card" style="width: 11rem; position: static; height: 100%; padding-bottom: 13px; box-shadow: 3px 3px 8px #adadad;" data-toggle="tooltip" data-placement="top" title="<?= $modules_data['m_name'] ?>" data-original-title="<?= $syllabus_data['s_course_title']; ?>" id="cardID">
                        <div class="border-checkbox-section" style="position: absolute; margin: 6px;">
                            <div class="border-checkbox-group border-checkbox-group-inverse">
                                <input class="border-checkbox" type="checkbox" onclick="countCheckboxes()" id="checkbox<?= $modules_data['m_id']; ?>" name="cbD" value="<?= $modules_data['m_id']; ?>">
                                <label class="border-checkbox-label" for="checkbox<?= $modules_data['m_id']; ?>"></label>
                            </div>
                        </div>

                        <div class="dropdown-inverse dropdown open">
                            <span id="m_menu" onclick="fm_menu()" class="btn btn-disable dropdown-toggle waves-effect waves-light " type="button" id="dropdown-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i style="font-size: 22px;" class="fa fa-ellipsis-v"></i></span>
                            <div class="dropdown-menu" aria-labelledby="dropdown-4" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                                <a class="dropdown-item waves-light waves-effect" href="#" onclick="renameModule('<?= $modules_data['m_id']; ?>','<?= $modules_data['m_name']; ?>')">Rename</a>
                                <a class="dropdown-item waves-light waves-effect" href="#" onclick="deleteSpecificModule('<?= $modules_data['m_id']; ?>')">Delete</a>
                                <a class="dropdown-item waves-light waves-effect" href="#">View Description</a>
                            </div>
                        </div>
                        <div onclick="viewModules('<?= $modules_data['m_id']; ?>')">
                            <div class="card-img-top" style="text-align-last: center;"><i class="fa fa-folder" style="font-size: 145px;color: #f8d775; margin-top: 10%;"></i></div>
                            <div class="card-body" style="text-align: center; padding: 0;">
                                <h5 class="card-title" style="margin-bottom: 10px;"><?= $modules_data['m_name'] ?></h5>

                                <p class="card-text" style="white-space: nowrap; width: 84%;  overflow: hidden; text-overflow: ellipsis; margin: 0 auto;"><?= $modules_data['m_desc'] ?></p>

                                <p class="card-text" style="white-space: nowrap; width: 84%;  overflow: hidden; text-overflow: ellipsis; margin: 0 auto;"><?= (Auth::user("role_id") == 1 ? getRole($modules_data['m_added_by']) : "") ?></p>
                                <p class="card-text" style="white-space: nowrap; width: 84%;  overflow: hidden; text-overflow: ellipsis; margin: 0 auto;"><?= date('F d, Y', strtotime($modules_data['m_date_added'])) ?></p>
                            </div>

                        </div>

                    </div>

                <?php }
            } else { ?>
                <div class="col-lg-12">

                    <div class="alert alert-warning icons-alert">

                        <p><strong>Aw snap!</strong> <code>Empty..</code></p>

                    </div>

                </div>


            <?php } ?>
        </div>
    </div>

    <?php include_once __DIR__ . '/modals/add_module_modals.php'; ?>
    <?php include_once __DIR__ . '/modals/rename_module_modals.php'; ?>

    <script>
        $(document).ready(function() {
            $("#bulkDeleteBtn").hide();
        });

        function addModule() {
            $("#moduleModal").modal();
        }

        function renameModule(id, m_name) {
            $("#renameModuleModal").modal();
            var m_id = $("#m_id").val(id);
            var m_name = $("#module_rename").val(m_name);
        }

        function viewModules(id) {
            window.location.href = base_url + '/modules/view/' + id;
        }

        function fm_menu() {
            //alert("test");
        }

        function countCheckboxes() {
            var inputElems = document.getElementsByTagName("input");
            count = 0;
            for (var i = 0; i < inputElems.length; i++) {
                if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
                    count++;

                }
            }

            if (count != 0) {
                $("#bulkDeleteBtn").show();
                $("#bulkDeleteBtn").addClass("animated bounceIn");
            } else {
                $("#bulkDeleteBtn").removeClass("animated bounceIn");
                $("#bulkDeleteBtn").hide();

                // $("#bulkDeleteBtn").addClass("animated bounceOut");
            }
        }

        $('#frmAddModule').submit(function(e) {
            e.preventDefault();
            var url = base_url + '/allfiles/save/module';
            var data = $('#frmAddModule').serialize();
            $.post(url, data, function(data) {
                if (data == 1) {

                    notify("top", "right", "fa fa-check", "success", "animated bounceInRight", "animated bounceOutRight", "<b>Success!</b> ", " Data has been Added.");
                    $("#moduleModal").modal('hide');
                    setTimeout(function() {
                        location.reload();
                    }, 1500);
                } else {
                    notify("top", "right", "fa fa-check", "danger", "animated bounceInRight", "animated bounceOutRight", "<b>Aw snap!</b> ", " Something went wrong");
                }

            });
        });

        $('#frmRenameModule').submit(function(e) {
            e.preventDefault();
            var url = base_url + '/allfiles/rename/module';
            var data = $('#frmRenameModule').serialize();
            $.post(url, data, function(data) {
                if (data == 1) {

                    notify("top", "right", "fa fa-check", "success", "animated bounceInRight", "animated bounceOutRight", "<b>Success!</b> ", " Data has been Updated.");
                    $("#renameModuleModal").modal('hide');
                    setTimeout(function() {
                        location.reload();
                    }, 1500);
                } else {
                    notify("top", "right", "fa fa-check", "danger", "animated bounceInRight", "animated bounceOutRight", "<b>Aw snap!</b> ", " Something went wrong");
                }

            });
        });

        function deleteBulkModule() {
            var popUp = confirm("Are you sure you want to delete this entry??");
            if (popUp == true) {
                if ($("input[name=cbD]").is(":checked")) {
                    var id = [];
                    var id = $("input[name=cbD]:checked").map(function() {
                        return $(this).val();
                    }).get();
                    $.ajax({
                        type: "POST",
                        url: base_url + "/allfiles/delete_bulk_module",
                        data: {
                            id: id
                        },
                        success: function(data) {
                            if (data == 1) {
                                location.reload();
                                notify("top", "right", "fa fa-check", "success", "animated bounceInRight", "animated bounceOutRight", "<b>Success!</b> ", " Record deleted.");
                                $("input[name=cbDAll]").prop("checked", false);
                            }
                        }
                    });
                } else {
                    notify("top", "right", "fa fa-check", "warning", "animated bounceInRight", "animated bounceOutRight", "<b>Aw Snap!</b> ", " No entry selected.");

                }
            } else {
                notify("top", "right", "fa fa-check", "warning", "animated bounceInRight", "animated bounceOutRight", "<b>Aw Snap!</b> ", " You've cancel it!");
            }
        }

        function deleteSpecificModule(id) {
            var popUp = confirm("Are you sure you want to delete this entry??");
            if (popUp == true) {
                $.ajax({
                    type: "POST",
                    url: base_url + "/allfiles/delete_specific_module",
                    data: {
                        id: id
                    },
                    success: function(data) {
                        if (data == 1) {
                            location.reload();
                            notify("top", "right", "fa fa-check", "success", "animated bounceInRight", "animated bounceOutRight", "<b>Success!</b> ", " Record deleted.");
                        }
                    }
                });
            } else {
                notify("top", "right", "fa fa-check", "warning", "animated bounceInRight", "animated bounceOutRight", "<b>Aw Snap!</b> ", " You've cancel it!");
            }
        }
    </script>
    <?php require 'layouts/footer.php'; ?>