<?php

use App\Core\Auth;

require 'layouts/head.php'; ?>
<div class="col-lg-12">
    <div class="col-md-8" style="margin: 0 auto; margin-bottom: 30px;">
        <img style="width: inherit;" src="<?= public_url('/assets/adminty/assets/images/bannerlogo.png') ?>" alt="banner-logo">
    </div>
    <div class="row align-items-end" style="margin-bottom: 10px;">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4><?= $pageTitle ?></h4>
                    <!-- <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span> -->
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <!-- <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="<?= route('/') ?>"> <i class="feather icon-home"></i> </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!"><?= $pageTitle ?></a>
                    </li>
                </ul>
            </div> -->
        </div>
    </div>
</div>

<!-- <div class="col-lg-12"> -->
<div class="col-md-12">
    <div class="card user-card-full">
        <div class="row m-l-0 m-r-0">
            <div class="col-sm-4 bg-c-lite-green user-profile">
                <div class="card-block text-center text-white">
                    <div class="m-b-25">
                        <img src="<?= public_url('/assets/adminty/assets/images/avatar-4.jpg') ?>" class="img-radius" alt="User-Profile-Image">
                    </div>
                    <h6 class="f-w-600"><?= Auth::user('fullname') ?></h6>
                    <p><?= getRole(Auth::user('role_id')); ?></p>
                    <!-- <i class="feather icon-edit m-t-10 f-16"></i> -->
                </div>
            </div>
            <div class="col-sm-8">
                <div class="card-block">
                    <h6 class="m-b-20 p-b-5 b-b-default f-w-600">Information</h6>
                    <div class="row">
                        <div class="col-sm-6">
                            <p class="m-b-10 f-w-600">Course</p>
                            <h6 class="text-muted f-w-400">Chemical Engineer</h6>
                        </div>
                        <div class="col-sm-6">
                            <p class="m-b-10 f-w-600">Subject</p>
                            <h6 class="text-muted f-w-400">English</h6>
                        </div>
                    </div>
                    <h6 class="m-b-20 m-t-40 p-b-5 b-b-default f-w-600">Time</h6>
                    <div class="row">
                        <div class="col-sm-6">
                            <p class="m-b-10 f-w-600">Start</p>
                            <h6 class="text-muted f-w-400">7:30 AM</h6>
                        </div>
                        <div class="col-sm-6">
                            <p class="m-b-10 f-w-600">End</p>
                            <h6 class="text-muted f-w-400">8:30 AM</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php require 'layouts/footer.php'; ?>