<?php

use App\Core\Auth;


require 'layouts/head.php'; ?>

<div class="col-lg-12">
    <div class="col-md-8" style="margin: 0 auto; margin-bottom: 30px;">
        <img style="width: inherit;" src="<?= public_url('/assets/adminty/assets/images/bannerlogo.png') ?>" alt="banner-logo">
    </div>
    <div class="row align-items-end" style="margin-bottom: 10px;">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4><?= $pageTitle ?></h4>
                    <!-- <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span> -->
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="<?= route('/') ?>"> <i class="feather icon-home"></i> Home </a>
                    </li>
                    <?= $breadcrumbs; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div>
    <button class="btn btn-inverse btn-icon pull-right" onclick="addSyllabus()" data-toggle="tooltip" data-placement="left" title="" data-original-title="Add Syllabus" style="position: absolute; right: 23px; box-shadow: 0px 2px 32px -3px rgb(34 34 34 / 75%); top: 86%; z-index: 1;  width: 60px;  height: 60px;"><i style="margin-right: 0 !important; font-size: 26px;" class="fa fa-plus"></i></button>
    <button class="btn btn-danger btn-icon pull-right" onclick="deleteSyllabus()" data-toggle="tooltip" data-placement="left" title="" data-original-title="Delete Syllabus" style="position: absolute; right: 99px; box-shadow: 0px 2px 32px -3px rgb(34 34 34 / 75%); top: 86%; z-index: 1;  width: 60px;  height: 60px;"><i style="margin-right: 0 !important; font-size: 26px;" class="fa fa-trash"></i></button>
</div>
<!-- Zero config.table start -->
<div class="col-lg-12">
    <div class="card">
        <!-- <div class="card-header">

            <button class="btn btn-out btn-danger btn-square waves-effect md-trigger float-right" onclick="deleteSyllabus()"><i class="fa fa-trash"></i>Delete Syllabus</button>
            <button class="btn btn-out btn-success btn-square waves-effect md-trigger float-right" onclick="addSyllabus()"><i class="fa fa-plus-circle"></i>Add Syllabus</button>
        </div> -->
        <!-- <div class="col-md-11" style="border-bottom: 2px dashed #404e67; margin: 0 auto;"></div> -->
        <div class="card-block">
            <div class="dt-responsive table-responsive">
                <table id="tbl_syllabus" class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th style="width: 36px;  border-top-left-radius: 16px !important; padding-bottom: 20px;">
                                <div class="checkbox-color checkbox-inverse">
                                    <input type="checkbox" id="checkboxAll" name="cbDAll" onclick="checkAll()">
                                    <label for="checkboxAll">

                                    </label>
                                </div>
                            </th>
                            <th>#</th>
                            <th></th>
                            <th>Subject ID</th>
                            <th>Subject Name</th>
                            <th>Description</th>
                            <th>Added By</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $counter = 1;
                        foreach ($details as $data) : ?>
                            <tr>
                                <td>

                                    <div class="checkbox-color checkbox-inverse">
                                        <input type="checkbox" id="checkbox<?= $data->subject_id; ?>" name="cbD" value="<?= $data->subject_id; ?>">
                                        <label for="checkbox<?= $data->subject_id; ?>">

                                        </label>
                                    </div>
                                </td>
                                <td><?= $counter++; ?></td>
                                <td>
                                    <div class="dropdown-inverse dropdown open">
                                        <button class="btn btn-inverse dropdown-toggle waves-effect waves-light" type="button" id="dropdown-7" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdown-7" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut"><a class="dropdown-item waves-light waves-effect" onclick="" href="<?= route('/allfiles/view/') . $data->subject_id ?> "><i class="zmdi zmdi-eye"></i> View Details</a><a class="dropdown-item waves-light waves-effect" onclick="editData('<?= $data->subject_id ?>')" href="#"><i class="zmdi zmdi-edit"></i> Edit Details</a></div>
                                    </div>
                                </td>
                                <td><?= $data->subject_id; ?></td>
                                <td><?= $data->subject_name; ?></td>
                                <td><?= $data->subject_desc; ?></td>
                                <td><?= getUser($data->subject_added_by); ?></td>

                            </tr>
                        <?php endforeach ?>
                    </tbody>

                </table>
            </div>
        </div>
        <!-- Zero config.table end -->


    </div>

    <?php include_once __DIR__ . '/modals/add_subject.php'; ?>
    <?php include_once __DIR__ . '/modals/edit_subject.php'; ?>
    <script>
        $(document).ready(function() {
            getSyllabusTable();
        });

        function getSyllabusTable() {
            // alert("test");
            $("#tbl_syllabus").DataTable();
        }

        function addSyllabus() {
            $("#subjectModal").modal();
        }

        $('#frmAddSubject').submit(function(e) {
            e.preventDefault();
            var url = base_url + '/sc/add';
            var data = $('#frmAddSubject').serialize();
            $.post(url, data, function(data) {
                if (data == 1) {

                    notify("top", "right", "fa fa-check", "success", "animated bounceInRight", "animated bounceOutRight", "<b>Success!</b> ", " Data has been updated.");
                    $("#subjectModal").modal('hide');
                    setTimeout(function() {
                        location.reload();
                    }, 1500);



                } else {
                    notify("top", "right", "fa fa-check", "danger", "animated bounceInRight", "animated bounceOutRight", "<b>Aw snap!</b> ", " Something went wrong");
                }

            });
        });

        function checkAll() {
            if ($("input[name=cbDAll]").is(":checked")) {
                $("input[name=cbD]").prop("checked", true);

            } else {
                $("input[name=cbD]").prop("checked", false);
            }

        }

        function editData(id) {
            //   alert(id);
            $("#editSubjectModal").modal();
            url = base_url + "/sc/edit";

            $.post(url, {
                id: id
            }, function(data) {
                var dd = JSON.parse(data);


                // console.log(dd);
                $("#id_subject_id").val(dd.subject_id);
                $("#id_subject_name").val(dd.subject_name);
                $("#id_subject_desc").val(dd.subject_desc);

            });
        }
        $('#frmEditSubject').submit(function(e) {
            e.preventDefault();
            var url = base_url + '/sc/update';
            var data = $('#frmEditSubject').serialize();
            $.post(url, data, function(response) {
                // alert(response);
                if (response == 1) {
                    location.reload();
                    notify("top", "right", "fa fa-check", "success", "animated bounceInRight", "animated bounceOutRight", "<b>Success!</b> ", " Record deleted.");
                    $("input[name=cbDAll]").prop("checked", false);
                }
            });
        });

        function deleteSyllabus() {
            var popUp = confirm("Are you sure you want to delete this entry??");
            if (popUp == true) {
                if ($("input[name=cbD]").is(":checked")) {
                    var id = [];
                    var id = $("input[name=cbD]:checked").map(function() {
                        return $(this).val();
                    }).get();

                    $.ajax({
                        type: "POST",
                        url: base_url + "/sc/delete",
                        data: {
                            id: id
                        },
                        success: function(data) {
                            // alert(data);
                            if (data == 1) {
                                location.reload();
                                notify("top", "right", "fa fa-check", "success", "animated bounceInRight", "animated bounceOutRight", "<b>Success!</b> ", " Record deleted.");
                                $("input[name=cbDAll]").prop("checked", false);
                            }
                        }
                    });

                } else {
                    notify("top", "right", "fa fa-check", "warning", "animated bounceInRight", "animated bounceOutRight", "<b>Aw Snap!</b> ", " No entry selected.");

                }
            } else {
                notify("top", "right", "fa fa-check", "warning", "animated bounceInRight", "animated bounceOutRight", "<b>Aw Snap!</b> ", " You've cancel it!");
            }
        }
    </script>
    <?php require 'layouts/footer.php'; ?>