<?php

use App\Core\Auth;

require 'layouts/head.php'; ?>
<style>
    .page-item.active .page-link {
        z-index: 0 !important;
    }

    .select2-container--default.select2-container--focus .select2-selection--multiple {
        height: 38px;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
    }
</style>
<div class="col-lg-12">
    <div class="col-md-8" style="margin: 0 auto; margin-bottom: 30px;">
        <img style="width: inherit;" src="<?= public_url('/assets/adminty/assets/images/bannerlogo.png') ?>" alt="banner-logo">
    </div>
    <div class="row align-items-end" style="margin-bottom: 10px;">
        <div class="col-lg-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4><?= $pageTitle ?></h4>
                    <!-- <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span> -->
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="<?= route('/') ?>"> <i class="feather icon-home"></i> </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!"><?= $pageTitle ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Zero config.table start -->
<div>
    <button class="btn btn-inverse btn-icon pull-right" onclick="addSchedule()" data-toggle="tooltip" data-placement="left" title="" data-original-title="Add Program" style="position: absolute; right: 23px; box-shadow: 0px 2px 32px -3px rgb(34 34 34 / 75%); top: 86%; z-index: 1;  width: 60px;  height: 60px;"><i style="margin-right: 0 !important; font-size: 26px;" class="fa fa-plus"></i></button>
    <button class="btn btn-danger btn-icon pull-right" onclick="deleteSchedule()" data-toggle="tooltip" data-placement="left" title="" data-original-title="Delete Program" style="position: absolute; right: 99px; box-shadow: 0px 2px 32px -3px rgb(34 34 34 / 75%); top: 86%; z-index: 1;  width: 60px;  height: 60px;"><i style="margin-right: 0 !important; font-size: 26px;" class="fa fa-trash"></i></button>
</div>
<div class="col-lg-12">
    <div class="card">

        <div class="card-block">
            <div class="dt-responsive table-responsive">
                <table id="tbl_schedule" class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th style="width: 36px;  border-top-left-radius: 16px !important; padding-bottom: 20px;">
                                <div class="checkbox-color checkbox-inverse">
                                    <input type="checkbox" id="checkboxAll" name="cbDAll" onclick="checkAll()">
                                    <label for="checkboxAll">

                                    </label>
                                </div>
                            </th>
                            <th>#</th>
                            <th></th>
                            <th>Course</th>
                            <th>Subject</th>
                            <th>Teacher</th>
                            <th>Start Time</th>
                            <th>End time</th>
                            <th style="border-top-right-radius: 16px !important;">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $counter = 1;
                        foreach ($details as $data) : ?>
                            <tr>
                                <td>
                                    <div class="checkbox-color checkbox-inverse">
                                        <input type="checkbox" id="checkbox<?= $data['sched_id']; ?>" name="cbD" value="<?= $data['sched_id']; ?>">
                                        <label for="checkbox<?= $data['sched_id']; ?>">

                                        </label>
                                    </div>
                                </td>
                                <td><?= $counter++; ?></td>
                                <td>

                                    <div id="toolbar-options<?= $data['sched_id']; ?>" class="hidden">
                                        <a href="#" onclick="editSched(<?= $data['sched_id']; ?>)"><i class="fa fa-pencil"></i></a>
                                    </div>
                                    <div class="tool-box">
                                        <div data-toolbar="user-options" class="btn-toolbar btn-inverse btn-toolbar-dark" id="dark-toolbar<?= $data['sched_id']; ?>" onmouseover="toolbar(<?= $data['sched_id']; ?>)"><i class="fa fa-cog"></i></div>
                                        <div class="clear"></div>
                                    </div>
                                </td>
                                <td><?= $data['sched_course']; ?></td>
                                <td><?= $data['sched_subject']; ?></td>
                                <td><?= $data['sched_teacher']; ?></td>
                                <td><?= date("g:i A", strtotime($data['sched_start_time'])); ?></td>
                                <td><?= date("g:i A", strtotime($data['sched_end_time'])); ?></td>
                                <td><?= $data['sched_desc']; ?></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>

                </table>
            </div>
        </div>
        <!-- Zero config.table end -->


    </div>

    <?php include_once __DIR__ . '/modals/add_schedule_modals.php'; ?>
    <?php include_once __DIR__ . '/modals/edit_schedule_modal.php'; ?>
    <script>
        $(document).ready(function() {
            getScheduleTable();


        });

        function toolbar(id) {
            // alert(id);
            $('#dark-toolbar' + id).toolbar({
                content: '#toolbar-options' + id,
                position: "left",
                style: 'dark'
            });
        }

        function getScheduleTable() {
            // alert("test");

            $("#tbl_schedule").DataTable();
        }

        function addSchedule() {
            //alert('test');
            $("#scheduleModal").modal();
        }

        function editSched(id) {
            // alert("test");
            $("#editscheduleModal").modal();
            url = base_url + "/schedule/edit";

            $.post(url, {
                id: id
            }, function(data) {
                var dd = JSON.parse(data);


                // console.log(dd);
                $("#id_sched_id").val(dd.sched_id);
                $("#id_course_name").val(dd.sched_course);
                $("#id_subject").val(dd.sched_subject);
                $("#id_teacher").val(dd.sched_teacher);
                $("#id_start_time").val(dd.sched_start_time);
                $("#id_end_time").val(dd.sched_end_time);
                $("#id_desc").val(dd.sched_desc);
                //  alert(dd.sched_start_time);
            });

        }
        $('#frmAddSchedule').submit(function(e) {
            e.preventDefault();
            var url = base_url + '/schedule/save';
            var data = $('#frmAddSchedule').serialize();
            $.post(url, data, function(data) {
                if (data == 1) {

                    notify("top", "right", "fa fa-check", "success", "animated bounceInRight", "animated bounceOutRight", "<b>Success!</b> ", " Data has been updated.");
                    $("#scheduleModal").modal('hide');
                    setTimeout(function() {
                        location.reload();
                    }, 1000);
                } else {
                    notify("top", "right", "fa fa-check", "danger", "animated bounceInRight", "animated bounceOutRight", "<b>Aw snap!</b> ", " Something went wrong");
                }

            });
        });

        $('#frmEditSchedule').submit(function(e) {
            e.preventDefault();
            var url = base_url + '/schedule/update';
            var data = $('#frmEditSchedule').serialize();
            $.post(url, data, function(data) {
                if (data == 1) {
                    location.reload();
                    notify("top", "right", "fa fa-check", "success", "animated bounceInRight", "animated bounceOutRight", "<b>Success!</b> ", " Record deleted.");
                    $("input[name=cbDAll]").prop("checked", false);
                }
            });
        });

        function checkAll() {
            if ($("input[name=cbDAll]").is(":checked")) {
                $("input[name=cbD]").prop("checked", true);

            } else {
                $("input[name=cbD]").prop("checked", false);
            }

        }

        function deleteSchedule() {
            // alert("test");
            var popUp = confirm("Are you sure you want to delete this entry??");
            if (popUp == true) {
                if ($("input[name=cbD]").is(":checked")) {
                    var id = [];
                    var id = $("input[name=cbD]:checked").map(function() {
                        return $(this).val();
                    }).get();

                    //alert(id);

                    $.ajax({
                        type: "POST",
                        url: base_url + "/schedule/delete",
                        data: {
                            id: id
                        },
                        success: function(data) {
                            // alert(data);
                            if (data == 1) {
                                location.reload();
                                notify("top", "right", "fa fa-check", "success", "animated bounceInRight", "animated bounceOutRight", "<b>Success!</b> ", " Record deleted.");
                                $("input[name=cbDAll]").prop("checked", false);
                            }
                        }
                    });

                } else {
                    notify("top", "right", "fa fa-check", "warning", "animated bounceInRight", "animated bounceOutRight", "<b>Aw Snap!</b> ", " No entry selected.");

                }
            } else {
                notify("top", "right", "fa fa-check", "warning", "animated bounceInRight", "animated bounceOutRight", "<b>Aw Snap!</b> ", " You've cancel it!");
            }
        }
    </script>
    <?php require 'layouts/footer.php'; ?>