<?php

namespace App\Controllers;

use App\Core\App;
use App\Core\Request;

class ScheduleController
{
    protected $pageTitle;

    public function schedule()
    {
        $pageTitle = "Course";
        $details = DB()->selectLoop("*", "tbl_schedule")->get();
        return view('/schedule', compact('details', 'pageTitle'));
    }
    public function save()
    {
        $resquest = Request::validate('/');
        $p_course = $resquest['course_name'];
        $p_subject = $resquest['subject'];
        $p_teacher = $resquest['teacher'];
        $p_start_time = $resquest['start_time'];
        $p_end_time = $resquest['end_time'];
        $p_desc = $resquest['desc'];

        $data = array(
            'sched_subject' => $p_subject,
            'sched_course' => $p_course,
            'sched_teacher' => $p_teacher,
            'sched_start_time' => $p_start_time,
            'sched_end_time' => $p_end_time,
            'sched_desc' => $p_desc
        );

        $result = DB()->insert("tbl_schedule", $data);
        echo $result;
    }
    public function delete()
    {
        // $resquest = Request::validate('/');
        $id = $_POST['id'];
        foreach ($id as $idarr) {
            $result = DB()->delete('tbl_schedule', "sched_id = '$idarr'");
            //redirect('/schedule', ["Deleted successfully.", 'success']);

        }
        echo $result;

        //var_dump($id);
    }
    public function edit()
    {

        $resquest = Request::validate('/');
        $id = $resquest['id'];


        $scheddata = DB()->select("*", 'tbl_schedule', "sched_id='$id'")->get();

        $list = array(
            "sched_id" => $scheddata['sched_id'],
            "sched_course" => $scheddata['sched_course'],
            "sched_subject" => $scheddata['sched_subject'],
            "sched_teacher" => $scheddata['sched_teacher'],
            "sched_start_time" => $scheddata['sched_start_time'],
            "sched_end_time" => $scheddata['sched_end_time'],
            "sched_desc" => $scheddata['sched_desc'],
        );

        // json_encode($scheddata)
        echo json_encode($list);
    }
    function update()
    {
        $resquest = Request::validate('/');
        $p_id = $resquest['sched_id'];
        $update_project_form = [

            "sched_subject" => $resquest['subject'],
            "sched_course" => $resquest['course_name'],
            "sched_teacher" => $resquest['teacher'],
            "sched_start_time" => $resquest['start_time'],
            "sched_end_time" => $resquest['end_time'],
            "sched_desc" => $resquest['desc'],

        ];

        $result = DB()->update("tbl_schedule", $update_project_form, "sched_id = '$p_id'");
        echo $result;
    }
}
