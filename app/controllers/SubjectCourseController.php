<?php

namespace App\Controllers;

use App\Core\App;
use App\Core\Auth;
use App\Core\Request;
use App\Core\Filesystem;

class SubjectCourseController
{
    protected $pageTitle;

    public function subject()
    {
        $pageTitle = "Manage Subject";
        $breadcrumbs = "<li class='breadcrumb-item'><a href='#'>" . $pageTitle . "</a></li>";
        $details = DB()->selectLoop("*", "tbl_subject")->get();
        return view('/subject', compact('details', 'pageTitle', 'breadcrumbs'));
    }
    public function course()
    {
        $pageTitle = "Manage Course";
        $details = DB()->selectLoop("*", "tbl_course")->get();
        return view('/course', compact('details', 'pageTitle'));
    }
    public function c_add()
    {
        $user_id     = Auth::user('id');
        $resquest = Request::validate('/');
        $course_name =  $resquest['course_name'];
        $course_desc =  $resquest['course_desc'];

        $data = array(
            'course_name' => $course_name,
            'course_desc' => $course_desc,
            'course_added_by' => $user_id,
        );

        $result = DB()->insert("tbl_course", $data);
        echo $result;
    }
    public function c_delete()
    {
        $id = $_POST['id'];
        foreach ($id as $idarr) {
            $result = DB()->delete('tbl_course', "course_id = '$idarr'");
        }
        echo $result;
    }
    public function c_edit()
    {
        $resquest = Request::validate('/');
        $id = $resquest['id'];


        $data = DB()->select("*", 'tbl_course', "course_id='$id'")->get();

        $list = array(
            "course_id" => $data['course_id'],
            "course_name" => $data['course_name'],
            "course_desc" => $data['course_desc'],


        );

        // json_encode($scheddata)
        echo json_encode($list);
    }
    function c_update()
    {
        $resquest = Request::validate('/');
        $p_id = $resquest['course_id'];
        $update_project_form = [
            "course_name" => $resquest['course_name'],
            "course_desc" => $resquest['course_desc'],
        ];

        $result = DB()->update("tbl_course", $update_project_form, "course_id = '$p_id'");
        echo $result;
    }
    public function add()
    {
        $user_id     = Auth::user('id');
        $resquest = Request::validate('/');
        $subject_name =  $resquest['subject_name'];
        $subject_desc =  $resquest['subject_desc'];

        $data = array(
            'subject_name' => $subject_name,
            'subject_desc' => $subject_desc,
            'subject_added_by' => $user_id,
        );

        $result = DB()->insert("tbl_subject", $data);
        echo $result;
    }
    public function edit()
    {
        $resquest = Request::validate('/');
        $id = $resquest['id'];


        $data = DB()->select("*", 'tbl_subject', "subject_id='$id'")->get();

        $list = array(
            "subject_id" => $data['subject_id'],
            "subject_name" => $data['subject_name'],
            "subject_desc" => $data['subject_desc'],


        );

        // json_encode($scheddata)
        echo json_encode($list);
    }
    public function delete()
    {
        $id = $_POST['id'];
        foreach ($id as $idarr) {
            $result = DB()->delete('tbl_subject', "subject_id = '$idarr'");
        }
        echo $result;
    }
    function update()
    {
        $resquest = Request::validate('/');
        $p_id = $resquest['subject_id'];
        $update_project_form = [
            "subject_name" => $resquest['subject_name'],
            "subject_desc" => $resquest['subject_desc'],
        ];

        $result = DB()->update("tbl_subject", $update_project_form, "subject_id = '$p_id'");
        echo $result;
    }
}
