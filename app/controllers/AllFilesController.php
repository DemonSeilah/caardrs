<?php

namespace App\Controllers;

use App\Core\App;
use App\Core\Auth;
use App\Core\Request;
use App\Core\Filesystem\Filesystem;

class AllFilesController
{
    protected $pageTitle;

    public function allfiles()
    {
        $user_id     = Auth::user('id');
        if (Auth::user('role_id') == 1) {
            $query = "";
        } else {
            $query = "s_added_by='$user_id'";
        }

        $pageTitle   = "All Files";
        $breadcrumbs = "<li class='breadcrumb-item'><a href='" . route('/allfiles/home') . "'>All Files</a></li>";
        $details     = DB()->selectLoop("*", "tbl_syllabus", "$query")->get();
        $schedule_details_data = DB()->selectLoop("*", 'tbl_schedule')->get();

        return view('/allfiles', compact('details', 'schedule_details_data', 'pageTitle', 'breadcrumbs'));
    }

    public function view_specfiles($id)
    {
        $user_id     = Auth::user('id');
        if (Auth::user('role_id') == 1) {
            $query = "";
        } else {
            $query = "AND m_added_by='$user_id'";
        }

        $pageTitle             = "View Files";
        $breadcrumbs           = "<li class='breadcrumb-item'><a href='" . route('/allfiles/home') . "'>All Files</a></li>
                                  <li class='breadcrumb-item'><a href='" . route('/allfiles/view/') . $id . "'>View Files</a></li>";
        $syllabus_data         = DB()->select("*", 'tbl_syllabus', "s_id='$id' ")->get();
        $syllabus_details_data = DB()->selectLoop("*", 'tbl_modules', "s_id='$id' $query")->get();
        return view('/viewfiles', compact('id', 'pageTitle', 'breadcrumbs', 'syllabus_data', 'syllabus_details_data', 'schedule_details_data'));
    }


    public function save()
    {
        $user_id            = Auth::user('id');
        $resquest           = Request::validate('/');
        $course_code           = $resquest['course_code'];
        $course_title           = $resquest['course_title'];
        $course_preq           = $resquest['course_preq'];
        $course_credit           = $resquest['course_credit'];
        $course_hrwk           = $resquest['course_hrwk'];
        $course_program           = $resquest['course_program'];



        $data = array(
            's_course_code'      => $course_code,
            's_course_title'     => $course_title,
            's_desc'             => $course_preq,
            's_credit_units'     => $course_credit,
            's_hours_week'       => $course_hrwk,
            'sched_id'           => $course_program,
            's_date_added' => date("Y-m-d"),
            's_added_by' => $user_id
        );

        $result = DB()->insert("tbl_syllabus", $data);
        echo $result;
    }

    public function save_module()
    {
        $user_id            = Auth::user('id');
        $resquest           = Request::validate('/');
        $s_id               = $resquest['s_id'];
        $module             = $resquest['module'];
        $description        = $resquest['description'];

        $userFolder = "public/assets/drive/{$module}";
        if (!Filesystem::exists($userFolder)) {
            Filesystem::makeDirectory($userFolder);
        }

        $data = array(
            's_id'         => $s_id,
            'm_name'       => $module,
            'm_desc'       => $description,
            'm_added_by'   => $user_id,
            'm_slug'       => $userFolder,
            'm_date_added' => date("Y-m-d")
        );

        $result = DB()->insert("tbl_modules", $data);
        echo $result;
    }

    public function rename_module()
    {
        $resquest           = Request::validate('/');
        $m_id               = $resquest['m_id'];
        $module_rename      = $resquest['module_rename'];

        $rename_data = array(
            'm_name'       => $module_rename
        );

        $result = DB()->update('tbl_modules', $rename_data, "m_id = '$m_id'");
        echo $result;
    }

    public function delete()
    {
        $id = $_POST['id'];
        foreach ($id as $idarr) {
            $result = DB()->delete('tbl_syllabus', "s_id = '$idarr'");
        }
        echo $result;
    }

    public function delete_bulk_module()
    {
        $id = $_POST['id'];
        $file = new Filesystem;

        foreach ($id as $idarr) {

            // FILES
            $users_files = DB()->selectLoop("*", "user_files", "folder_id = '$idarr'")->get();
            foreach ($users_files as $files) {
                if (Filesystem::exists($files['slug'])) {
                    $file->delete($files['slug']);
                    DB()->delete("user_files", "id = '$files[id]'");
                }
            }

            // FOLDER
            $sub_folder_list = DB()->selectLoop("*", "tbl_modules", "m_id = '$idarr'")->get();
            foreach ($sub_folder_list as $sub_folder) {
                if (Filesystem::exists($sub_folder->m_slug)) {
                    $file->deleteDirectory($sub_folder->m_slug);
                }
            }

            $result = DB()->delete('tbl_modules', "m_id = '$idarr'");
        }

        echo $result;
    }

    public function delete_specific_module()
    {
        $id = $_POST['id'];
        $file = new Filesystem;

        // FILES
        $users_files = DB()->selectLoop("*", "user_files", "folder_id = '$id'")->get();
        foreach ($users_files as $files) {
            if (Filesystem::exists($files['slug'])) {
                $file->delete($files['slug']);
                DB()->delete("user_files", "id = '$files[id]'");
            }
        }

        // FOLDER
        $sub_folder_list = DB()->selectLoop("*", "tbl_modules", "m_id = '$id'")->get();
        foreach ($sub_folder_list as $sub_folder) {
            if (Filesystem::exists($sub_folder->m_slug)) {
                $file->deleteDirectory($sub_folder->m_slug);
            }
        }

        $result = DB()->delete('tbl_modules', "m_id = '$id'");
        echo $result;
    }
}
