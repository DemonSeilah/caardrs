<?php

namespace App\Controllers;

use App\Core\App;
use App\Core\Auth;
use App\Core\Request;
use App\Core\Filesystem;

class ModulesController
{
    protected $pageTitle;

    public function view_module($id)
    {

        $module_data         = DB()->select("*", 'tbl_modules', "m_id='$id' ")->get();
        $prev_module_id      = $module_data['s_id'];
        $files               = DB()->selectLoop("*", "user_files", "folder_id = '$id'")->get();
        $pageTitle           = "View Module Files";
        $breadcrumbs         = "<li class='breadcrumb-item'><a href='" . route('/allfiles/view/') . $prev_module_id . "'>View Files</a></li>
                                  <li class='breadcrumb-item'><a href='" . route('/modules/view/') . $id . "'>Module Files</a></li>";
        return view('/module_view', compact('id', 'pageTitle', 'breadcrumbs', 'module_data', 'files'));
    }

    public function uploadFile($code)
    {
        $user_id     = Auth::user('id');
        $module_data = DB()->select("*", 'tbl_modules', "m_id='$code' ")->get();
        $module_name = $module_data['m_name'];

        if (Request::hasFile('upload_file')) {
            $file_tmp = $_FILES['upload_file']['tmp_name'];

            $file_name = explode('_', str_replace(array('.', ' ', ',', '-'), '_', $_FILES['upload_file']['name']));
            array_pop($file_name);

            $fileSize = $_FILES['upload_file']['size'] / 1000000;
            $file_type = explode('.', $_FILES['upload_file']['name']);
            $file_type_end = end($file_type);
            $fileType = strtoupper($file_type_end);

            $filename = strtoupper(randChar(4)) . date('Ymdhis') . "." . $file_type_end;

            $image_icons_array = array("JPEG", "JPG", "EXIF", "TIFF", "GIF", "BMP", "PNG", "SVG", "ICO", "PPM", "PGM", "PNM");
            if (in_array($fileType, $image_icons_array)) {
                $previewSize = '100%';
            } else {
                $previewSize = '25%';
            }

            $folder = uniqid() . '-' . date('Ymdhis');
            $temp_dir = "public/assets/drive/{$module_name}/";

            $folderName = implode('_', $file_name);
            $this->saveFilesInDb($user_id, $code, $temp_dir . $filename, $folderName, $fileSize, $fileType, $previewSize);

            Request::storeAs($file_tmp, $temp_dir, $_FILES['upload_file']['type'], $filename);

            echo $folder;
        }

        echo '';
    }

    public function saveFilesInDb($user_id, $folder_id, $path, $file_name, $fileSize, $fileType, $previewSize)
    {
        $fileCode = randChar(3) . date('ymdhis');

        $data_form = [
            "file_code" => $fileCode,
            "user_id" => $user_id,
            "folder_id" => $folder_id,
            "slug" => $path,
            "filetype" => $fileType,
            "filename" => $file_name,
            "filesize" => $fileSize,
            "iconsize" => $previewSize,
            "created_at" => date('Y-m-d h:i:s'),
            "updated_at" => date('Y-m-d h:i:s')
        ];

        DB()->insert("user_files", $data_form);
    }
}
