<?php

namespace App\Controllers;

use App\Core\App;
use App\Core\Auth;
use App\Core\Request;

class UserController
{
    protected $pageTitle;

    public function user()
    {
        $pageTitle = "Users";
        $details = DB()->selectLoop("*", "users")->get();
        return view('/user', compact('details', 'pageTitle'));
    }

    public function save()
    {
        $resquest = Request::validate('/user/home', [
            'fullname' => ['required'],
            'email' => ['required', 'email', 'unique:users'],
            'username' => ['required', 'unique:users'],
        ]);

        $fullname = $resquest['fullname'];
        $username = $resquest['username'];
        $email = $resquest['email'];
        $role = $resquest['role'];
        $password = bcrypt("a");


        $data = array(
            'email' => $email,
            'fullname' => $fullname,
            'username' => $username,
            'password' => $password,
            'role_id' => $role,
            'created_at' => date("Y-m-d")
        );

        $result = DB()->insert("users", $data);
        echo $result;
    }
    public function edit()
    {

        $resquest = Request::validate('/');
        $id = $resquest['id'];
        $scheddata = DB()->select("*", 'users', "id='$id'")->get();
        $list = array(
            "id" => $scheddata['id'],
            "email" => $scheddata['email'],
            "fullname" => $scheddata['fullname'],
            "username" => $scheddata['username'],
            "role_id" => $scheddata['role_id'],
            "create_at" => $scheddata['create_at'],
        );

        // json_encode($scheddata)
        echo json_encode($list);
    }
    public function delete()
    {
        $id = $_POST['id'];
        foreach ($id as $idarr) {
            $result = DB()->delete('users', "id = '$idarr'");
        }
        echo $result;
    }
}
