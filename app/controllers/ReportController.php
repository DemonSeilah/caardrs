<?php

namespace App\Controllers;

use App\Core\App;
use App\Core\Auth;
use App\Core\Request;

class ReportController
{
    protected $pageTitle;

    public function report()
    {
        $pageTitle = "Reports";
       
        return view('/report',compact('pageTitle'));
    }

}
